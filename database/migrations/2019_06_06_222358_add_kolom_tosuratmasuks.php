<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKolomTosuratmasuks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('suratmasuks',function ($table){
            $table->string('nomersurat')->after('jenis_surat');
            $table->string('perihal')->after('nomersurat')->nullable();
            $table->string('kepada')->after('perihal')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('suratmasuks',function ($table){
            $table->dropColumn('nomersurat');
            $table->dropColumn('perihal');
            $table->dropColumn('kepada');
           
        });
        
    }
}
