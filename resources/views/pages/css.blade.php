
	<!-- Main Styles -->
	<link rel="stylesheet" href="{{URL::asset('assets/styles/style-horizontal.min.css')}}">

	<!-- mCustomScrollbar -->
	<link rel="stylesheet" href="{{URL::asset('assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.min.css')}}">

	<!-- Waves Effect -->
	<link rel="stylesheet" href="{{URL::asset('assets/plugin/waves/waves.min.css')}}">

	<!-- Sweet Alert -->
	<link rel="stylesheet" href="{{URL::asset('assets/plugin/sweet-alert/sweetalert.css')}}">
	
	<!-- Percent Circle -->
	<link rel="stylesheet" href="{{URL::asset('assets/plugin/percircle/css/percircle.css')}}">

	<!-- Chartist Chart -->
	<link rel="stylesheet" href="{{URL::asset('assets/plugin/chart/chartist/chartist.min.css')}}">

	<!-- FullCalendar -->
	<link rel="stylesheet" href="{{URL::asset('assets/plugin/fullcalendar/fullcalendar.min.css')}}">
	<link rel="stylesheet" href="{{URL::asset('assets/plugin/fullcalendar/fullcalendar.print.css')}}" media='print'>

	<!-- Color Picker -->
	<link rel="stylesheet" href="{{URL::asset('assets/color-switcher/color-switcher.min.css')}}">
	<link rel="stylesheet" href="{{URL::asset('assets/plugin/modal/remodal/remodal.css')}}">
	<link rel="stylesheet" href="{{URL::asset('assets/plugin/modal/remodal/remodal-default-theme.css')}}">

