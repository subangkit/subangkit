	<!-- Placed at the end of the document so the pages load faster -->
	 <script src="{{URL::asset('assets/scripts/jquery.min.js')}}"></script> 

	<!--  <script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous"></script> -->

  
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->
	<script src="{{URL::asset('assets/scripts/modernizr.min.js')}}"></script>
	<script src="{{URL::asset('assets/plugin/bootstrap/js/bootstrap.min.js')}}"></script>
	
	<script src="{{URL::asset('assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js')}}"></script>
	<script src="{{URL::asset('assets/plugin/nprogress/nprogress.js')}}"></script>
	<script src="{{URL::asset('assets/plugin/sweet-alert/sweetalert.min.js')}}"></script>
	<script src="{{URL::asset('assets/plugin/waves/waves.min.js')}}"></script>
	<!-- Full Screen Plugin -->
	<script src="{{URL::asset('assets/plugin/fullscreen/jquery.fullscreen-min.js')}}"></script>

	<!-- Percent Circle -->
	<script src="{{URL::asset('assets/plugin/percircle/js/percircle.js')}}"></script>

	<!-- Google Chart -->
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

	<!-- Chartist Chart -->
	<script src="{{URL::asset('assets/plugin/chart/chartist/chartist.min.js')}}"></script>
	<script src="{{URL::asset('assets/scripts/chart.chartist.init.min.js')}}"></script>

	<!-- FullCalendar -->
	<script src="{{URL::asset('assets/plugin/moment/moment.js')}}"></script>
	<script src="{{URL::asset('assets/plugin/fullcalendar/fullcalendar.min.js')}}"></script>
	<script src="{{URL::asset('assets/scripts/fullcalendar.init.js')}}"></script>

	<script src="{{URL::asset('assets/scripts/main.min.js')}}"></script>
	<script src="{{URL::asset('assets/scripts/horizontal-menu.min.js')}}"></script>
	<script src="{{URL::asset('assets/color-switcher/color-switcher.min.js')}}"></script>