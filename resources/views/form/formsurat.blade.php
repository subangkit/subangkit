@extends('layout.layout')
@push('css')
<link href="{{ asset('css/datatable.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/plugin/datepicker/css/bootstrap-datepicker.min.css')}}">

@endpush
@section('content')
<div class="col-lg-6 col-xs-12">
				<div class="box-content card white">
					<h4 class="box-title">Form Surat</h4>
					<!-- /.box-title -->
					<div class="card-content">
						<button class="btn btn-success btn-sm" id="addsurat">Tambah Surat</button>
						<button class="btn btn-warning btn-sm" id="ctksurat">Cetak Surat | Agenda</button>
					</div>
					<!-- /.card-content -->
				</div>
				<!-- /.box-content -->
</div>

<div class="col-lg-12 col-xs-12">
				<div class="box-content card white">
					<h4 class="box-title">Data Surat Masuk</h4>
					<!-- /.box-title -->
					<div class="card-content">
                        <!-- open tabel -->
						<table class="table" id="suratmasuk">
							<thead>
								<tr>
									<th>No </th>
									<th>Tanggal</th>
									<th>Dari</th>
									<th>Kepentingan</th>
									<th></th>
								</tr>
							</thead>
							
						</table>
                        <!-- end tabel -->
						
					</div>
					<!-- /.card-content -->
				</div>
				<!-- /.box-content -->
</div>
@stop
@section('modal')
<!-- modal disposisi -->
<div class="modal fade" id="modaldisposisi"  role="dialog" aria-labelledby="myModalLabel-1">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				
				<h4 class="modal-title" id="myModalLabel-1"></h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
				<!-- table -->
				<div class="col-lg-6 col-xs-12">
				
					<h5 class="box-title">Disposisi </h5>
					<!-- /.box-title -->
					
						<form class="form-horizontal">
							<div class="form-group">
								<label for="inp-type-1" class="col-sm-3 control-label">Text</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="inp-type-1" placeholder="Some text value ...">
								</div>
							</div>
							<div class="form-group">
								<label for="inp-type-2" class="col-sm-3 control-label">Email</label>
								<div class="col-sm-9">
									<input type="email" class="form-control" id="inp-type-2" placeholder="Email address">
								</div>
							</div>
							<div class="form-group">
								<label for="inp-type-3" class="col-sm-3 control-label">Password</label>
								<div class="col-sm-9">
									<input type="password" class="form-control" id="inp-type-3" placeholder="Password" value="Password">
								</div>
							</div>
							<div class="form-group">
								<label for="inp-type-4" class="col-sm-3 control-label">Placeholder</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="inp-type-4" placeholder="Placeholder">
								</div>
							</div>
							<div class="form-group">
								<label for="inp-type-5" class="col-sm-3 control-label">Textarea</label>
								<div class="col-sm-9">
									<textarea class="form-control" id="inp-type-5" placeholder="Write your meassage"></textarea>
								</div>
							</div>
						</form>
					
					<!-- /.card-content -->
				
				<!-- /.box-content card white -->
			</div>
				

	
				</div>
			</div>
			<div class="modal-footer" >
				
				
			</div>
		</div>
	</div>
</div>
<!-- end modal disposisi -->


<div class="modal fade" id="modalfile"  role="dialog" aria-labelledby="myModalLabel-1">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				
				<h4 class="modal-title" id="myModalLabel-1">Berkas File</h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
				<!-- table -->
				<table class="table table-hover" id="tabel">
						<thead>
							<tr>
								<th>No</th>
								<th>Keterangan Berkas</th> 
								<th>Tanggal Upload</th> 
								<th></th> 
							</tr> 
						</thead> 
						<tbody> 
						</tbody> 
					</table>
	
				</div>
			</div>
			<div class="modal-footer" >
				
				
			</div>
		</div>
	</div>
</div>
<!-- modal file -->
<div class="modal fade" id="cetaksurat"  role="dialog" aria-labelledby="myModalLabel-1">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				
				<h4 class="modal-title" id="myModalLabel-1">Cetak Surat | Agenda</h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
					<form action="{{route('surat.masuk')}}" class="form-horizontal" enctype="multipart/form-data" method="post">
					{{csrf_field()}}
					<div class="col-md-6">
					<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label"></label>
								<div class="col-sm-10" id="datepicker">
									<div class="input-group date" data-provide="datepicker">
										<input type="text" class="form-control" name="tanggal_surat">
										<div class="input-group-addon">
											<span class="glyphicon glyphicon-th"></span>
										</div>
									</div>
								</div>
					</div>
					</div>
					<div class="col-md-6">
					<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label">Sampai</label>
								<div class="col-sm-10" id="datepicker">
									<div class="input-group date" data-provide="datepicker">
										<input type="text" class="form-control" name="tanggal_surat">
										<div class="input-group-addon">
											<span class="glyphicon glyphicon-th"></span>
										</div>
									</div>
								</div>
					</div>
					</div>	
				</div>
			</div>
			<div class="modal-footer">
				<button type="reset" class="btn btn-warning waves-effect waves-light">RESET</button>
				<button type="submit" class="btn btn-info waves-effect waves-light">CETAK</button>
				
			</div>
			</form>
		</div>
	</div>
</div>
<!-- modal add data -->
<div class="modal fade" id="tambahSurat"  role="dialog" aria-labelledby="myModalLabel-1">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				
				<h4 class="modal-title" id="myModalLabel-1">Form Surat Masuk</h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
				<form action="{{route('surat.masuk')}}" class="form-horizontal" enctype="multipart/form-data" method="post">
				{{csrf_field()}}

				<div class="col-md-6">
					<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">Nomer Agenda</label>
								<div class="col-sm-10">
									<input type="agenda" class="form-control" id="noagenda" placeholder="" name="noagenda">
								</div>
					</div>
					<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label">Tanggal</label>
								<div class="col-sm-10" id="datepicker">
								<div class="input-group date" data-provide="datepicker">
									<input type="text" class="form-control" name="tanggal_surat">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-th"></span>
									</div>
								</div>
								</div>
					</div>

					<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label">Dari</label>
								<div class="col-sm-10">
									<input type="test" class="form-control" name="dari" id="inputPassword3" placeholder="">
								</div>
					</div>
					<div class="form-group">
								<label for="jenis surat" class="col-sm-2 control-label">Jenis Surat</label>
								<div class="col-sm-10">
								<select class="form-control" name="jenis_surat">
									<option value="">-- Pilih --</option>
									<option value="biasa">Biasa</option>
									<option value="penting">Penting</option>
									<option value="sangatpenting">Sangat Penting</option>
								</select>
								</div>
								
					</div>
				</div>

				<div class="col-md-6">
				<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label">Nomer Surat</label>
								<div class="col-sm-10">
									<input type="test" class="form-control" name="nmrsurat" id="inputPassword3" placeholder="">
								</div>
				</div>
				<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label">Perihal</label>
								<div class="col-sm-10">
									<input type="test" class="form-control" name="perihal" id="inputPassword3" placeholder="">
								</div>
				</div>
				<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label">Kepada</label>
								<div class="col-sm-10">
									<input type="test" class="form-control" name="kepada" id="inputPassword3" placeholder="">
								</div>
					</div>
				<div class="form-group">
								<label for="upload file">Upload Berkas</label>
								<input type="file" id="file" name="berkas">
								<p class="help-block">Tipe File PDF atau Gambar</p>
				</div>
				</div>
				</div>	
			</div>
			<div class="modal-footer">
			<button type="reset" class="btn btn-warning waves-effect waves-light">RESET</button>
			<button type="submit" class="btn btn-info waves-effect waves-light">SIMPAN</button>
				
			</div>
			</form>
		</div>
	</div>
</div
@stop

@section('js')
<script src="{{URL::asset('assets/plugin/datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{URL::asset('js/datatables.js')}}"></script>
<script type="text/javascript">
	$(function() {
		$('#suratmasuk').DataTable({
			processing: true,
			serverSide: true,
			ajax: 'list',
			columns: [
			{ data: 'agenda', name: 'agenda' },
			// {data: 'DT_RowIndex', name: 'DT_RowIndex'},
			{ data: 'tanggal_surat', name: 'tanggal_surat' },
			{ data: 'dari', name: 'dari' },
			{ data: 'jenis_surat', name: 'jenis_surat' },
			{ data: 'action', name: 'action', orderable: false, searchable: false},
			]
		});
	});
	$.fn.dataTable.ext.errMode = 'throw';
$("#addsurat").click(function(){
	// load idagenda terakhir
	$.ajax({
		type: 'GET',
		url:"{{URL::route('noagenda')}}",
		success: function(data){
			console.log(data);
			$("input[name=noagenda]").val(data['data']);
		}
	});
	$("#tambahSurat").modal("show");	
})

function lihatDetailFile(id){
	var buatTable ='';//lihat file
	$.ajax({
		type:'GET',
		url:"{{URL('file/ambilfile')}}"+'/'+id,
		success:function(data){
		// console.log(data);	
		var jmlData = data['data'].length;
		for(i=0;i<jmlData;i++){

			// buatTable +="<tr><td>"+(i+1)+"</td><td><a href='{{URL::asset('berkas')}}/"+data['data'][i]['nama']+"'>"+data['data'][i]['nama']+"</a></td><td>data['data'][i]['tanggal_upload']</td></tr>";
			
			buatTable +="<tr>"
									+"<td>"+(i+1)+"</td>"
    	 							+"<td><a href='{{URL::asset('berkas')}}/"+data['data'][i]['nama']+"'>"+data['data'][i]['nama']+"</a></td>"
    	 							+"<td>"+data['data'][i]['tanggal_uplod']+"</td>"
					  +"<tr>";

		}
		$("#modalfile #tabel tbody").append(buatTable);
		$("#modalfile").modal("show");
		console.log(buatTable);
		}
	});
	$("#modalfile #tabel tbody").html("");
}
// disposisi
function adddisposisi(id){
	// alert(id)
	$("#modaldisposisi").modal("show");

}

// cetak surat atau agenda
$("#ctksurat").click(function(){
	$("#cetaksurat").modal('show');
})

function hapussurat(id){//belu sukses mungkin harus pakai form 
	confirm("Apakah anda yakin untuk menghapus file ini?");
	$.ajax({
			type:"DELETE",
			// url('ajax-crud')}}"+'/'+user_id
			url:"{{URL('home/suratmasuk')}}"+'/'+id,
			success:function(data){
				console.log(data);
			}
	});

}

</script>
@stop