<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suratmasuk extends Model
{
    //
    protected $table = 'suratmasuks';

    public function getFiles(){
        return $this->hasMany('App\File','surat_id');
    }


}
