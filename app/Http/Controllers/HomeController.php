<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\Suratmasuk; //model
use Illuminate\Http\Request;
use Validator; //validasi
use DB; //database
use File; //hapus file
class HomeController extends Controller
{
    public function index(){
        return view('layout.layout');
    }
    public function surat(){
        return view('form.formsurat');
    }
    public function table(){
        $classes = Suratmasuk::select(['id', 'agenda', 'tanggal_surat', 'dari','jenis_surat']);
        return DataTables::of($classes)
        ->addIndexColumn()
        ->addColumn('action', function($subjects){
        return '<a href="class/'.$subjects->id.'/edit" class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o"></i> </a>
        <a href="#" class="btn btn-danger btn-xs" data-toggle="modal" onclick="hapussurat('.$subjects->id.')"><i class="fa fa-trash-o"></i> </a>
        <a href="#" class="btn btn-success btn-xs" data-toggle="modal" onclick="adddisposisi('.$subjects->id.')"><i class="fa fa-archive"></i> </a>
        <a href="#" class="btn btn-info btn-xs" data-toggle="modal" onclick="lihatDetailFile('.$subjects->id.')""><i class="fa fa-info"></i> </a>';
        })
        ->make(true);   
            }
    public function noagenda(){
        $no = DB::select('SELECT COUNT(agenda) as no FROM suratmasuks');
        $nomeragenda = $no[0]->no+1;
        return response()->json(['data'=>$nomeragenda]);
    }
    public function insert(Request $request){//insert
        if($request['jenis_surat'] == ''){
           dd('jenis_surat harus diisi');
        }
        // tanggal
        $input = $request['tanggal_surat'];
        $dt = Carbon::parse($input);
        $tanggal = $dt->year.'-'.$dt->month.'-'.$dt->day;
        $no = $request['noagenda'];
        $dari = $request['dari'];
        $jenissurat = $request['jenis_surat'];
        // insert surat
        $suratmasuk = DB::table('suratmasuks')->insert([
            'agenda' => $no,
            'tanggal_surat' => $tanggal,
            'dari' => $dari,
            'jenis_surat' => $jenissurat,
            'perihal' => $request['perihal'],
            'nomersurat' => $request['nmrsurat'],
            'kepada' => $request['kepada'],
            "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
             ]);
        // ambil surat id
        $qyStr = "SELECT id from suratmasuks ORDER BY id DESC  LIMIT 1";
        $result = DB::select($qyStr);
        foreach($result as $data){
            $surat_id = $data->id;
        }
        $validator = Validator::make($request->all(), [
            'berkas' => 'required|image|mimes:jpeg,pdf,png,jpg,gif,svg|max:3048', //rule
          ]);
        //validasi jika variabel berkas ada
        if(isset($request->berkas)){ 
            $namagambar = $jenissurat.'-'.time().'.'.$request->berkas->getClientOriginalExtension();
            // pindahkan file ke folder 
            $request->berkas->move(public_path('berkas'),$namagambar);
            $user_id = 10;
            $tgluplod = date('Y-m-d');
            // insert file
            $insertfile = DB::table('files')->insert([
                'nama' => $namagambar,
                'tanggal_uplod' => $tgluplod,
                'user_id' => $user_id,
                'surat_id' => $surat_id,
                "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
            ]);
            return redirect('home/suratmasuk');
            
        }else{
            return redirect('home/suratmasuk');
        }
    }
    // hapus surat
    public function hapus($id){
        $data = Suratmasuk::findOrFail($id)->delete();
        dd($data);
    }

    //lihat file/berkas
    public function getFile($id){
        $berkas = DB::select('select * from files where surat_id='.$id);
        return response()->json(['data'=>$berkas]);
    }
}
